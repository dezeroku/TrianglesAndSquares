import javax.swing.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;



class BaseClass {

    public static void main(String[] args) {

        GraphicalUserInterface graphical = new GraphicalUserInterface();

        JFrame mainwin = new JFrame("Okno glowne");
        mainwin.setBounds(400,400,400,400);
        mainwin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainwin.setVisible(true);
        mainwin.setResizable(false);
       
        mainwin.add(graphical.window);
        
        graphical.initGUI();
        mainwin.pack();



    }

}


class GraphicalUserInterface {

    public int begin_xsize = 500;							//length of the drawing panel
    public int begin_ysize = 500;							//width of the drawing panel

    public int xsize = begin_xsize;
    public int ysize = begin_ysize;
    public int drawing_color = Color.BLACK.getRGB(); 		//that color defines, which color will be used to draw figure (it's useful for cutting)

    public JInternalFrame window;

    public double begin_multiplier = 10;					//how big should be the figures
    public double multiplier = begin_multiplier;


    public JPanel main_panel;								//panel containing whole GUI

    public JPanel choose_panel;								//panel in which you can choose figure
    public JPanel checkers_panel;							//panel in which all Checkboxes are stored within
    public JPanel options_panel;							//panel in which all JTextFields are stored within

    private JComboBox<String> choose_figure;  				//deprecated at a time, contatins figures to choose from, looks just ugly


    public JPanel rectangle_panel;							//stores whole rectangle GUI
    public JTextField rectangle_length;
    public JTextField rectangle_width;
    public JCheckBox rectangle_display_length;
    public JCheckBox rectangle_display_width;
    public JCheckBox rectangle_display_diagonal;
    public JCheckBox rectangle_display_diagonal_length;
    public JButton rectangle_draw;							//button to draw predefined rectangle


    public JPanel square_panel;								//stores whole square GUI
    public JTextField square_length;
    public JCheckBox square_display_length;
    public JCheckBox square_display_diagonal;
    public JCheckBox square_display_diagonal_length;
    public JButton square_draw;								//button to draw predefined square


    public JPanel triangle_panel;
    public JComboBox<String> triangle_define_method;
    public JTextField triangle_height;
    public JTextField triangle_base;
    public JTextField triangle_one;
    public JTextField triangle_two;
    public JTextField triangle_three;
    public JTextField triangle_first_arm;
    public JTextField triangle_second_arm;
    public JTextField triangle_arm_angle;
    public JCheckBox triangle_display_height;
    public JCheckBox triangle_display_base;
    public JCheckBox triangle_display_first_arm;
    public JCheckBox triangle_display_second_arm;
    public JCheckBox triangle_display_angle;
    public JButton triangle_draw;


    public JPanel circle_panel;								//stores whole circle GUI
    public JTextField circle_radius;
    public JCheckBox circle_display_radius;
    public JCheckBox circle_display_radius_length;
    public JButton circle_draw;								//button to draw predefined square


    public JPanel save_panel;								//most bottom panel, contains save button
    public JButton save_button;								//button to save

    public JPanel imagining_panel;
    public JLabel image_onto_panel;
    public BufferedImage image_panel;


    GraphicalUserInterface() {

        init_save_panel();

        init_figure_panels();

        init_image_panel();

        init_choose_panel();

        init_main_panel();

        init_window();



        

    }

    private void init_textfields_sizes() {

        square_length.setPreferredSize(square_length.getSize());

        circle_radius.setPreferredSize(circle_radius.getSize());

        triangle_height.setPreferredSize(triangle_height.getSize());
        triangle_base.setPreferredSize(triangle_base.getSize());
        triangle_one.setPreferredSize(triangle_one.getSize());
        triangle_two.setPreferredSize(triangle_two.getSize());
        triangle_three.setPreferredSize(triangle_three.getSize());
        triangle_first_arm.setPreferredSize(triangle_first_arm.getSize());
        triangle_second_arm.setPreferredSize(triangle_second_arm.getSize());
        triangle_arm_angle.setPreferredSize(triangle_arm_angle.getSize());

        rectangle_length.setPreferredSize(rectangle_length.getSize());
        rectangle_width.setPreferredSize(rectangle_width.getSize());
    }

    private void init_figure_panels() {

        checkers_panel = new JPanel();
        checkers_panel.setLayout(new FlowLayout());


        rectangle_panel = new JPanel();
        rectangle_panel.setLayout(new FlowLayout());
        rectangle_length = new JTextField("Tu wprowadz dlugosc");
        rectangle_length.addFocusListener(new UniversalTextFieldListener());
        rectangle_width = new JTextField("Tu wprowadz szerokosc");
        rectangle_width.addFocusListener(new UniversalTextFieldListener());
        rectangle_display_width = new JCheckBox("szerokosc", true);
        rectangle_display_length = new JCheckBox("dlugosc", true);
        rectangle_display_diagonal = new JCheckBox("przekatna", true);
        rectangle_display_diagonal_length = new JCheckBox("dlugosc przekatnej", true);
        rectangle_draw = new JButton("Rysuj");
        rectangle_draw.addActionListener(new RectangleDrawButtonListener(this));



        rectangle_panel.add(rectangle_length);
        rectangle_panel.add(rectangle_width);
        checkers_panel.add(rectangle_display_length);
        checkers_panel.add(rectangle_display_width);
        checkers_panel.add(rectangle_display_diagonal);
        checkers_panel.add(rectangle_display_diagonal_length);
        rectangle_panel.add(rectangle_draw);

        square_panel = new JPanel();
        square_panel.setLayout(new FlowLayout());
        square_length = new JTextField("Tu wprowadz dlugosc boku");

        square_length.addFocusListener(new UniversalTextFieldListener());
        square_display_length = new JCheckBox("dlugosc", true);
        square_display_diagonal = new JCheckBox("przekatna", true);
        square_display_diagonal_length = new JCheckBox("dlugosc przekatnej", true);
        square_draw = new JButton("Rysuj");
        square_draw.addActionListener(new SquareDrawButtonListener(this));



        square_panel.add(square_length);
        checkers_panel.add(square_display_length);
        checkers_panel.add(square_display_diagonal);
        checkers_panel.add(square_display_diagonal_length);

        square_panel.add(square_draw);


        triangle_panel = new JPanel();
        triangle_panel.setLayout(new FlowLayout());
        triangle_define_method = new JComboBox<String>();
        triangle_define_method.addItem("Podstawa + wysokosc");
        triangle_define_method.addItem("Ramiona");
        triangle_define_method.addItem("Boki");
        triangle_define_method.addItemListener(new ItemChangeListenerTriangle(this));
        triangle_height = new JTextField("Tu wprowadz wysokosc");
        triangle_height.addFocusListener(new UniversalTextFieldListener());
        triangle_base = new JTextField("Tu wprowadz podstawe");
        triangle_base.addFocusListener(new UniversalTextFieldListener());
        triangle_one = new JTextField("Tu wprowadz pierwszy bok");
        triangle_one.addFocusListener(new UniversalTextFieldListener());
        triangle_two = new JTextField("Tu wprowadz drugi bok");
        triangle_two.addFocusListener(new UniversalTextFieldListener());
        triangle_three = new JTextField("Tu wprowadz trzeci bok");
        triangle_three.addFocusListener(new UniversalTextFieldListener());
        triangle_first_arm = new JTextField("Tu wprowadz pierwsze ramię");
        triangle_first_arm.addFocusListener(new UniversalTextFieldListener());
        triangle_second_arm = new JTextField("tu wprowadz drugie ramię");
        triangle_second_arm.addFocusListener(new UniversalTextFieldListener());
        triangle_arm_angle = new JTextField("Tu wprowadz kat miedzy ramionami");
        triangle_arm_angle.addFocusListener(new UniversalTextFieldListener());



        triangle_display_height = new JCheckBox("wysokosc", true);
        triangle_display_base = new JCheckBox("dlugosc podstawy", true);
        triangle_display_first_arm = new JCheckBox("dlugosc pierwszego ramienia", true);
        triangle_display_second_arm = new JCheckBox("dlugosc drugiego ramienia", true);
        triangle_display_angle = new JCheckBox("kat miedzy ramionami", true);
        triangle_draw = new JButton("Rysuj");

        triangle_panel.add(triangle_define_method);
        triangle_panel.add(triangle_base);
        triangle_panel.add(triangle_height);
        triangle_panel.add(triangle_one);
        triangle_panel.add(triangle_two);
        triangle_panel.add(triangle_three);
        triangle_panel.add(triangle_first_arm);
        triangle_panel.add(triangle_second_arm);
        triangle_panel.add(triangle_arm_angle);
        checkers_panel.add(triangle_display_height);
        checkers_panel.add(triangle_display_base);
        checkers_panel.add(triangle_display_first_arm);
        checkers_panel.add(triangle_display_second_arm);
        checkers_panel.add(triangle_display_angle);
        triangle_panel.add(triangle_draw);

        // triangle_base.setVisible(false);
        // triangle_height.setVisible(false);
        triangle_two.setVisible(false);
        triangle_one.setVisible(false);
        triangle_three.setVisible(false);
        triangle_first_arm.setVisible(false);
        triangle_second_arm.setVisible(false);
        triangle_display_angle.setVisible(false);
        triangle_arm_angle.setVisible(false);
        triangle_arm_angle.setVisible(false);
        triangle_display_angle.setVisible(false);
        // triangle_display_base.setVisible(false);
        // triangle_display_height.setVisible(false);
        triangle_display_first_arm.setVisible(false);
        triangle_display_second_arm.setVisible(false);
        //  triangle_draw.setVisible(false);



        circle_panel = new JPanel();
        circle_panel.setLayout(new FlowLayout());
        circle_radius = new JTextField("Tu wprowadz promien");
        circle_radius.addFocusListener(new UniversalTextFieldListener());
        circle_display_radius = new JCheckBox("Wyswietl promien", true);
        circle_display_radius_length = new JCheckBox("Wyswietl dlugosc promienia", true);
        circle_draw = new JButton("Rysuj");
        circle_draw.addActionListener(new CircleDrawButtonListener(this));

        circle_panel.add(circle_radius);
        checkers_panel.add(circle_display_radius);
        checkers_panel.add(circle_display_radius_length);
        circle_panel.add(circle_draw);







    }

    private void init_window() {

        window = new JInternalFrame();

        window.add(main_panel);

        window.setBounds(1000, 1000, 1000, 1000);
        window.setVisible(true);
        window.setResizable(false);



    }

    private void init_save_panel() {

        save_panel = new JPanel();
        save_button = new JGradientButton("Zapisz obraz");
        save_button.addActionListener(new SaveButtonListener(this));

        save_panel.add(save_button);
        save_button.setEnabled(false);

    }

    private void init_image_panel() {

        image_panel = new BufferedImage(xsize, ysize, BufferedImage.TYPE_USHORT_GRAY);

        image_onto_panel = new JLabel(new ImageIcon(image_panel));

        imagining_panel = new JPanel();
        imagining_panel.setPreferredSize(new Dimension(begin_xsize, begin_ysize));

        imagining_panel.add(image_onto_panel);

        invertColors();

    }

    private void init_main_panel() {

        main_panel = new JPanel();
        main_panel.setLayout(new BoxLayout(main_panel, BoxLayout.PAGE_AXIS));
        main_panel.setVisible(true);

        main_panel.add(choose_panel);
        main_panel.add(options_panel);
        main_panel.add(checkers_panel);
        main_panel.add(imagining_panel);
        main_panel.add(save_panel);

    }

    private void init_choose_panel() {

    	setSquareCheckersVisible(false);
    	setTriangleCheckersVisible(false);
    	setRectangleCheckersVisible(false);
    	setCircleCheckersVisible(false);


       

        choose_panel = new JPanel();
        choose_panel.setLayout(new FlowLayout());
        //choose_figure = new JComboBox<String>();
        //choose_figure.addItem("Prostokat");
        //choose_figure.addItem("Trojkat");
        //choose_figure.addItem("Kwadrat");
        // choose_figure.addItem("Kolo");

        //choose_panel.add(choose_figure);
        //
        //

        JButton triangle_button = new JButton("Trojkat");
        JButton circle_button = new JButton("Kolo");
        JButton rectangle_button = new JButton("Prostokat");
        JButton square_button = new JButton("Kwadrat");
        JButton about_button = new JButton("O programie");

        Graphics2D to_be_drawn = image_panel.createGraphics();





        triangle_button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                rectangle_panel.setVisible(false);
                square_panel.setVisible(false);
                circle_panel.setVisible(false);
                triangle_panel.setVisible(false);

                //temp.choose_panel.add(temp.rectangle_panel);


                setCircleCheckersVisible(false);
                setTriangleCheckersVisible(false);
                setRectangleCheckersVisible(false);
                setSquareCheckersVisible(false);


                setTriangleCheckersVisible(true);
                triangle_panel.setVisible(true);

                triangle_height.setText("Tu wprowadz wysokosc");
                triangle_base.setText("Tu wprowadz podstawe");
                triangle_one.setText("Tu wprowadz pierwszy bok");
                triangle_two.setText("Tu wprowadz drugi bok");
                triangle_three.setText("Tu wprowadz trzeci bok");
                triangle_first_arm.setText("Tu wprowadz pierwsze ramię");
                triangle_second_arm.setText("tu wprowadz drugie ramię");
                triangle_arm_angle.setText("Tu wprowadz kat miedzy ramionami");

                save_button.setBackground(square_draw.getBackground());
                save_button.setText("Zapisz obraz");


                to_be_drawn.clearRect(0, 0, begin_xsize * 2, begin_ysize * 2);
                invertColors();
                imagining_panel.revalidate();
                imagining_panel.repaint();

            }
        });

        circle_button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                rectangle_panel.setVisible(false);
                square_panel.setVisible(false);
                circle_panel.setVisible(false);
                triangle_panel.setVisible(false);

                //temp.choose_panel.add(temp.rectangle_panel);


                setCircleCheckersVisible(false);
                setTriangleCheckersVisible(false);
                setRectangleCheckersVisible(false);
                setSquareCheckersVisible(false);

                setCircleCheckersVisible(true);
                circle_panel.setVisible(true);

                circle_radius.setText("Tu wprowadz promien");

                save_button.setBackground(square_draw.getBackground());
                save_button.setText("Zapisz obraz");

                to_be_drawn.clearRect(0, 0, begin_xsize * 2, begin_ysize * 2);
                invertColors();
                imagining_panel.revalidate();
                imagining_panel.repaint();
            }
        });

        rectangle_button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                rectangle_panel.setVisible(false);
                square_panel.setVisible(false);
                circle_panel.setVisible(false);
                triangle_panel.setVisible(false);

                //temp.choose_panel.add(temp.rectangle_panel);


                setCircleCheckersVisible(false);
                setTriangleCheckersVisible(false);
                setRectangleCheckersVisible(false);
                setSquareCheckersVisible(false);

                setRectangleCheckersVisible(true);
                rectangle_panel.setVisible(true);

                rectangle_length.setText("Tu wprowadz dlugosc");
                rectangle_width.setText("Tu wprowadz szerokosc");

                save_button.setBackground(square_draw.getBackground());
                save_button.setText("Zapisz obraz");

                to_be_drawn.clearRect(0, 0, begin_xsize * 2, begin_ysize * 2);
                invertColors();
                imagining_panel.revalidate();
                imagining_panel.repaint();
            }
        });

        square_button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                rectangle_panel.setVisible(false);
                square_panel.setVisible(false);
                circle_panel.setVisible(false);
                triangle_panel.setVisible(false);

                //temp.choose_panel.add(temp.rectangle_panel);


                setCircleCheckersVisible(false);
                setTriangleCheckersVisible(false);
                setRectangleCheckersVisible(false);
                setSquareCheckersVisible(false);


                setSquareCheckersVisible(true);
                square_panel.setVisible(true);

                square_length.setText("Tu wprowadz dlugosc boku");

                save_button.setBackground(square_draw.getBackground());
                save_button.setText("Zapisz obraz");

                to_be_drawn.clearRect(0, 0, begin_xsize * 2, begin_ysize * 2);
                invertColors();
                imagining_panel.revalidate();
                imagining_panel.repaint();
            }
        });

        about_button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(null, "Stworzony przez: d0ku (Jakub Piątkowski)\n" +
                                              "2017", "O programie", JOptionPane.INFORMATION_MESSAGE);

            }
        });





        choose_panel.add(rectangle_button);
        choose_panel.add(square_button);
        choose_panel.add(circle_button);
        choose_panel.add(triangle_button);
        choose_panel.add(about_button);





        //
        //
        //
        //

        options_panel = new JPanel();
        options_panel.setLayout(new FlowLayout());



        options_panel.add(square_panel);
        options_panel.add(triangle_panel);
        options_panel.add(rectangle_panel);
        options_panel.add(circle_panel);

        rectangle_panel.setVisible(false);
        square_panel.setVisible(false);
        circle_panel.setVisible(false);
        triangle_panel.setVisible(false);



        //     choose_figure.addItemListener(new ItemChangeListener(this));


        options_panel.setPreferredSize(new Dimension(begin_xsize, 40));
        checkers_panel.setPreferredSize(new Dimension(begin_xsize, 40));

    }

    private void setSquareCheckersVisible(Boolean temp) {

        if(temp) {
            square_display_length.setVisible(true);
            square_display_diagonal.setVisible(true);
            square_display_diagonal_length.setVisible(true);

        } else {
            square_display_length.setVisible(false);
            square_display_diagonal.setVisible(false);
            square_display_diagonal_length.setVisible(false);
        }
    }

    private void setRectangleCheckersVisible(Boolean temp) {

        if(temp) {
            rectangle_display_length.setVisible(true);
            rectangle_display_width.setVisible(true);
            rectangle_display_diagonal.setVisible(true);
            rectangle_display_diagonal_length.setVisible(true);

        } else {
            rectangle_display_length.setVisible(false);
            rectangle_display_width.setVisible(false);
            rectangle_display_diagonal.setVisible(false);
            rectangle_display_diagonal_length.setVisible(false);

        }
    }

    private void setTriangleCheckersVisible(Boolean temp) {

        if(temp) {
            triangle_display_height.setVisible(true);
            triangle_display_base.setVisible(true);
            triangle_display_first_arm.setVisible(true);
            triangle_display_second_arm.setVisible(true);
            triangle_display_angle.setVisible(true);

        } else {
            triangle_display_height.setVisible(false);
            triangle_display_base.setVisible(false);
            triangle_display_first_arm.setVisible(false);
            triangle_display_second_arm.setVisible(false);
            triangle_display_angle.setVisible(false);
        }
    }

    private void setCircleCheckersVisible(Boolean temp) {


        if(temp) {
            circle_display_radius.setVisible(true);
            circle_display_radius_length.setVisible(true);
        } else {
            circle_display_radius.setVisible(false);
            circle_display_radius_length.setVisible(false);
        }
    }

    public void initGUI() {
    	
        window.pack();
        init_textfields_sizes();


    }

    /**
     * It cuts borders of image, refering to draw_color provided above
     * @param  original image to cut
     * @return          modified image
     */
    public BufferedImage cutBorders(BufferedImage original) {
        xsize = begin_xsize;
        ysize = begin_ysize;

        BufferedImage modified = original;
        modified = cutUp(modified);
        modified = cutDown(modified);
        modified = cutLeft(modified);
        modified = cutRight(modified);


        return modified;
    }

    private BufferedImage cutUp(BufferedImage original) {


        BufferedImage modified = original;

        int tempx = xsize;
        int tempy = ysize;
        int cut_up = 0;

        for(int j = 0; j < ysize; j++) {
            for(int i = 0; i < xsize; i++) {

                if(modified.getRGB(i, j) == drawing_color) {

                    cut_up = j;

                    ysize = ysize - cut_up;
                    j = ysize + 2 * cut_up;
                    break;
                }

            }
        }



        modified = modified.getSubimage(0, cut_up, xsize, ysize);

        return modified;
    }

    private BufferedImage cutDown(BufferedImage original) {


        BufferedImage modified = original;
        int tempx = xsize;
        int tempy = ysize;
        int cut_down = 0;

        for(int j = ysize - 1; j > 0 ; j--) {
            for(int i = 0; i < xsize; i++) {

                if(modified.getRGB(i, j) == drawing_color) {

                    cut_down = j + 1;

                    ysize = cut_down;
                    j = 0;
                    break;
                }

            }
        }


        modified = modified.getSubimage(0, 0, xsize, cut_down);

        return modified;
    }

    private BufferedImage cutLeft(BufferedImage original) {


        BufferedImage modified = original;
        int tempx = xsize;
        int tempy = ysize;
        int cut_left = 0;

        for(int i = 0; i < xsize; i++) {
            for(int j = 0; j < ysize ; j++) {

                if(modified.getRGB(i, j) == drawing_color) {

                    cut_left = i;

                    xsize = xsize - cut_left;
                    i = xsize + cut_left * 2;
                    j = ysize;
                    break;
                }

            }
        }


        modified = modified.getSubimage(cut_left, 0, xsize, ysize);

        return modified;
    }

    private BufferedImage cutRight(BufferedImage original) {


        BufferedImage modified = original;

        int tempx = xsize;
        int tempy = ysize;
        int cut_right = 0;

        for(int i = xsize - 1; i > 0; i--) {
            for(int j = 0; j < ysize ; j++)   {

                if(modified.getRGB(i, j) == drawing_color) {

                    cut_right = i + 1;

                    xsize = cut_right;
                    i = 0;
                    break;
                }

            }
        }



        modified = modified.getSubimage(0, 0, cut_right, ysize);

        return modified;
    }

    /**
     * Changes multiplier, so the whole image fits onto BufferedImage
     * @param x square length
     */
    public void squaremultiplier(float x) {

        while(20 + 20 + 20 + this.multiplier * x > this.begin_xsize || 20 + 20 + this.multiplier * x > this.begin_ysize  ) {
            multiplier *= 0.95;
        }
    }

    /**
     * Changes multiplier, so the whole image fits onto BufferedImage
     * @param x rectangle length
     * @param y rectangle width
     */
    public void rectanglemultiplier(float x, float y) {

        while(20 + 20 + 20 + this.multiplier * x > this.begin_xsize || 20 + 20 + this.multiplier * y > this.begin_ysize  ) {
            multiplier *= 0.95;
        }
    }

    /**
     * Changes multiplier, so the whole image fits onto BufferedImage
     * @param r circle radius
     */
    public void circlemultiplier(float r) {

        while(20 + 20 + ( this.multiplier * r) * 2 > this.begin_xsize || 20 + 20 + ( this.multiplier * r) * 2 > this.begin_ysize  ) {
            multiplier *= 0.95;
        }
    }

    /**
     * It restores the original multiplier
     */
    public void restoremultiplier() {
        this.multiplier = this.begin_multiplier;
    }

    /**
     * Inverts black and white colors onto BufferedImage
     */
    public void invertColors() {

        for(int i = 0; i < begin_xsize ; i++ ) {
            for(int j = 0; j < begin_ysize; j++) {
                if (image_panel.getRGB(i, j) == Color.BLACK.getRGB()) {
                    image_panel.setRGB(i, j, Color.WHITE.getRGB());
                } else if (image_panel.getRGB(i, j) == Color.WHITE.getRGB()) {
                    image_panel.setRGB(i, j, Color.BLACK.getRGB());
                }

            }
        }
    }

    @SuppressWarnings("serial")
    private static final class JGradientButton extends JButton {

        private JGradientButton(String text) {
            super(text);
            setContentAreaFilled(false);
        }

        @Override
        protected void paintComponent(Graphics g) {
            Graphics2D g2 = (Graphics2D)g.create();
            g2.setPaint(new GradientPaint(
                            new Point(0, 0),
                            getBackground(),
                            new Point(0, getHeight() / 3),
                            Color.WHITE));
            g2.fillRect(0, 0, getWidth(), getHeight() / 3);
            g2.setPaint(new GradientPaint(
                            new Point(0, getHeight() / 3),
                            Color.WHITE,
                            new Point(0, getHeight()),
                            getBackground()));
            g2.fillRect(0, getHeight() / 3, getWidth(), getHeight());
            g2.dispose();

            super.paintComponent(g);
        }
    }

}


class UniversalTextFieldListener implements FocusListener {

    public void focusGained(FocusEvent e) {

        JTextField temp = JTextField.class.cast( e.getSource());
        temp.setText("");
    }

    public void focusLost(FocusEvent e) {}
}

class SquareDrawButtonListener implements ActionListener {

    GraphicalUserInterface temp;

    public void actionPerformed(ActionEvent e) {

        float x = 0;
        Graphics2D to_be_drawn = temp.image_panel.createGraphics();

        to_be_drawn.clearRect(0, 0, temp.begin_xsize * 2, temp.begin_ysize * 2);
        to_be_drawn.setFont(new Font("default", Font.BOLD, 16));

        try {
            x = Float.parseFloat(temp.square_length.getText());
        } catch(Exception ex) {
            temp.square_length.setText("niepoprawna dlugosc");
            System.out.println("niepoprawna długość");
            return;
        }


        temp.squaremultiplier(x);


        to_be_drawn.draw(new Rectangle(20, 20, (int)(x * temp.multiplier), (int)(x * temp.multiplier)));

        if(temp.square_display_length.isSelected()) {
            if(x % 1 != 0) {
                to_be_drawn.drawString(Float.toString(x), (float)(20 + 2 + x * temp.multiplier), (float)( 20 + (x / 2) * temp.multiplier));
            } else {
                to_be_drawn.drawString(Integer.toString((int)x), (float)(20 + 2 + x * temp.multiplier), (float)( 20 + (x / 2) * temp.multiplier));
            }
        }


        if(temp.square_display_diagonal.isSelected()) {
            to_be_drawn.drawLine(20, (int)(20 + x * temp.multiplier), (int)(20 + x * temp.multiplier), 20);
        }

        if(temp.square_display_diagonal_length.isSelected()) {
            AffineTransform affineTransform = new AffineTransform();
            affineTransform.rotate(Math.toRadians(315), 0, 0);
            Font rotatedFont = to_be_drawn.getFont().deriveFont(affineTransform);
            to_be_drawn.setFont(rotatedFont);
            if(x % 1 != 0) {
                to_be_drawn.drawString(Float.toString(x) + "√2", (float)(20 + 2 + x * temp.multiplier / 2), (float)(20 + x * temp.multiplier / 2));
            } else {
                to_be_drawn.drawString(Integer.toString((int)x) + "√2", (float)(20 + 2 + x * temp.multiplier / 2), (float)( 20 + x * temp.multiplier / 2));
            }

        }

        temp.invertColors();
        temp.imagining_panel.revalidate();
        temp.imagining_panel.repaint();

        temp.restoremultiplier();
        temp.save_button.setEnabled(true);

    }

    SquareDrawButtonListener(GraphicalUserInterface temp) {
        this.temp = temp;
    }
}

class RectangleDrawButtonListener implements ActionListener {

    GraphicalUserInterface temp;

    public void actionPerformed(ActionEvent e) {


        float x = 0;
        float y = 0;
        Graphics2D to_be_drawn = temp.image_panel.createGraphics();

        to_be_drawn.clearRect(0, 0, temp.begin_xsize * 2, temp.begin_ysize * 2);
        to_be_drawn.setFont(new Font("default", Font.BOLD, 16));

        try {
            x = Float.parseFloat(temp.rectangle_length.getText());

        } catch(Exception ex) {

            temp.rectangle_length.setText("niepoprawna dlugosc");
            System.out.println("niepoprawna długość");
            return;
        }

        try {
            y = Float.parseFloat(temp.rectangle_width.getText());

        } catch(Exception ex) {

            temp.rectangle_width.setText("niepoprawna szerokosc");
            System.out.println("niepoprawna długość");
            return;
        }


        temp.rectanglemultiplier(x, y);


        to_be_drawn.draw(new Rectangle(20, 20, (int)(x * temp.multiplier), (int)(y * temp.multiplier)));

        if(temp.rectangle_display_width.isSelected()) {
            if(x % 1 != 0) {
                to_be_drawn.drawString(Float.toString(y), (float)(20 + 2 + x * temp.multiplier), (float)(20 + (y / 2) * temp.multiplier));
            } else {
                to_be_drawn.drawString(Integer.toString((int)y), (float)(20 + 2 + x * temp.multiplier), (float)(20 + (y / 2) * temp.multiplier));
            }
        }

        if(temp.rectangle_display_length.isSelected()) {
            if(x % 1 != 0) {
                to_be_drawn.drawString(Float.toString(x), (float)(20 +   (x * temp.multiplier) / 2), (float)(20 + 2 + y  * temp.multiplier));
            } else {
                to_be_drawn.drawString(Integer.toString((int)x), (float)(20 +   (x * temp.multiplier) / 2), (float)(20 + 2 + y  * temp.multiplier));
            }
        }


        if(temp.rectangle_display_diagonal.isSelected()) {
            to_be_drawn.drawLine(20, (int)(20 + y * temp.multiplier), (int)(20 + x * temp.multiplier), 20);
        }

        if(temp.rectangle_display_diagonal_length.isSelected()) {
            AffineTransform affineTransform = new AffineTransform();
            affineTransform.rotate(Math.toRadians(360 - Math.toDegrees(Math.atan(y / x))), 0, 0);
            Font rotatedFont = to_be_drawn.getFont().deriveFont(affineTransform);
            to_be_drawn.setFont(rotatedFont);
            if(Math.hypot(x, y) % 1 != 0) {
                to_be_drawn.drawString(Float.toString((float)Math.hypot(x, y)) , (float)( 20 + 2 + x * temp.multiplier / 2), (float)( 20 + y * temp.multiplier / 2));
            } else {
                to_be_drawn.drawString(Integer.toString((int)Math.hypot(x, y)) , (float)( 20 + 2 + x * temp.multiplier / 2), (float)( 20 + y * temp.multiplier / 2));
            }
            //to_be_drawn.drawString(Float.toString(x) + "√2", 20 + 2 + x * temp.multiplier / 2, 20 + x * temp.multiplier / 2);
        }




        temp.invertColors();
        temp.imagining_panel.revalidate();
        temp.imagining_panel.repaint();

        temp.restoremultiplier();
        temp.save_button.setEnabled(true);



    }

    RectangleDrawButtonListener(GraphicalUserInterface temp) {
        this.temp = temp;
    }
}

class CircleDrawButtonListener implements ActionListener {

    GraphicalUserInterface temp;

    public void actionPerformed(ActionEvent e) {



        float r = 0;

        Graphics2D to_be_drawn = temp.image_panel.createGraphics();

        to_be_drawn.clearRect(0, 0, temp.begin_xsize * 2, temp.begin_ysize * 2);
        to_be_drawn.setFont(new Font("default", Font.BOLD, 16));

        try {
            r = Float.parseFloat(temp.circle_radius.getText());

        } catch(Exception ex) {
            temp.circle_radius.setText("niepoprawny promien");
            System.out.println("niepoprawny promien");
            return;
        }

        temp.circlemultiplier(r);


        to_be_drawn.draw(new Ellipse2D.Double(20, 20, 2 * r * temp.multiplier, 2 * r * temp.multiplier));

        if(temp.circle_display_radius.isSelected()) {
            to_be_drawn.drawLine((int)(20 + r * temp.multiplier), (int)(20 + r * temp.multiplier), (int)(20 + r * temp.multiplier * 2), (int)(20 + r * temp.multiplier));
        }


        if(temp.circle_display_radius_length.isSelected()) {
            if(r % 1 != 0) {
                to_be_drawn.drawString(Float.toString(r), (int)(20 + 2 + r * temp.multiplier * 1.5), (int)(18 + r  * temp.multiplier ));
            } else {
                to_be_drawn.drawString(Integer.toString((int)r), (int)(20 + 2 + r * temp.multiplier * 1.5), (int)(18 + r  * temp.multiplier ));
            }
        }


        temp.invertColors();
        temp.imagining_panel.revalidate();
        temp.imagining_panel.repaint();

        temp.restoremultiplier();
        temp.save_button.setEnabled(true);


    }

    CircleDrawButtonListener(GraphicalUserInterface temp) {
        this.temp = temp;
    }
}

/*
class ItemChangeListener implements ItemListener {
    GraphicalUserInterface temp;

    public void itemStateChanged(ItemEvent event) {

        if (event.getStateChange() == ItemEvent.SELECTED) {
            String item = (String) event.getItem();



            switch(item) {

            case "Prostokat": {
                temp.rectangle_panel.setVisible(false);
                temp.square_panel.setVisible(false);
                temp.circle_panel.setVisible(false);
                temp.triangle_panel.setVisible(false);

                //temp.choose_panel.add(temp.rectangle_panel);

                temp.rectangle_panel.setVisible(true);

                break;
            }

            case "Kwadrat": {
                temp.rectangle_panel.setVisible(false);
                temp.square_panel.setVisible(false);
                temp.circle_panel.setVisible(false);
                temp.triangle_panel.setVisible(false);

                //  temp.choose_panel.add(temp.square_panel);

                temp.square_panel.setVisible(true);
                break;
            }

            case "Trojkat": {

                temp.rectangle_panel.setVisible(false);
                temp.square_panel.setVisible(false);
                temp.circle_panel.setVisible(false);
                temp.triangle_panel.setVisible(false);

                //  temp.choose_panel.add(temp.triangle_panel);

                temp.triangle_panel.setVisible(true);
                break;
            }

            case "Kolo": {

                temp.rectangle_panel.setVisible(false);
                temp.square_panel.setVisible(false);
                temp.circle_panel.setVisible(false);
                temp.triangle_panel.setVisible(false);

                //  temp.choose_panel.add(temp.circle_panel);

                temp.circle_panel.setVisible(true);
                break;
            }
            }
            //temp.window.pack();
        }
    }

    ItemChangeListener(GraphicalUserInterface temp) {
        this.temp = temp;
    }
}

*/
class ItemChangeListenerTriangle implements ItemListener {
    GraphicalUserInterface temp;

    public void itemStateChanged(ItemEvent event) {

        if (event.getStateChange() == ItemEvent.SELECTED) {
            String item = (String) event.getItem();


            switch(item) {

            case "Boki": {
                temp.triangle_first_arm.setVisible(false);
                temp.triangle_second_arm.setVisible(false);
                temp.triangle_base.setVisible(false);
                temp.triangle_height.setVisible(false);
                temp.triangle_display_angle.setVisible(false);
                temp.triangle_arm_angle.setVisible(false);

                temp.triangle_two.setVisible(true);
                temp.triangle_one.setVisible(true);
                temp.triangle_three.setVisible(true);


                break;
            }

            case "Podstawa + wysokosc": {
                temp.triangle_two.setVisible(false);
                temp.triangle_one.setVisible(false);
                temp.triangle_three.setVisible(false);
                temp.triangle_first_arm.setVisible(false);
                temp.triangle_second_arm.setVisible(false);
                temp.triangle_display_angle.setVisible(false);
                temp.triangle_arm_angle.setVisible(false);

                temp.triangle_base.setVisible(true);
                temp.triangle_height.setVisible(true);

                break;
            }

            case "Ramiona": {
                temp.triangle_two.setVisible(false);
                temp.triangle_one.setVisible(false);
                temp.triangle_three.setVisible(false);
                temp.triangle_base.setVisible(false);
                temp.triangle_height.setVisible(false);

                temp.triangle_first_arm.setVisible(true);
                temp.triangle_second_arm.setVisible(true);
                temp.triangle_display_angle.setVisible(true);
                temp.triangle_arm_angle.setVisible(true);


                break;
            }


            }
            //temp.window.pack();
        }
    }

    ItemChangeListenerTriangle(GraphicalUserInterface temp) {
        this.temp = temp;
    }
}



class SaveButtonListener implements ActionListener {


    GraphicalUserInterface temp;

    public void actionPerformed(ActionEvent e) {


        try {

            JFileChooser chooser = new JFileChooser();

            File workingDirectory = new File(System.getProperty("user.dir"));
            chooser.setCurrentDirectory(workingDirectory);

            int returnVal = chooser.showSaveDialog(temp.window);
            if(returnVal == JFileChooser.APPROVE_OPTION) {
                System.out.println("You chose to save this file: " +
                                   chooser.getSelectedFile());

                File outputfile = new File(chooser.getSelectedFile() + ".png");
                BufferedImage to_save = temp.cutBorders(temp.image_panel);
                ImageIO.write(to_save, "png", outputfile);
                System.out.println("Zapis");

            }


        } catch (IOException ex) {
            System.out.println("Nie zapisalo sie");

            temp.save_button.setBackground(Color.RED);
            temp.save_button.setText("ERROR");



        }

        temp.save_button.setEnabled(false);

    }

    SaveButtonListener(GraphicalUserInterface temp) {
        this.temp = temp;
    }
}

